
function each(elements, cb) {
    for(let idx in elements){
        cb(elements[idx]);
    }
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
}
module.exports=each;