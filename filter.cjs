function filter(elements, cb) {
    let new_arr=[];
    for(let idx=0; idx<elements.length; idx++){
        if(cb(elements[idx],idx,elements)==true){
            new_arr.push(elements[idx]);
        }
    }
    return new_arr;
}
module.exports=filter;