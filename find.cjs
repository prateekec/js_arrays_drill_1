function find(elements, cb) {
    for(let idx=0; idx<elements.length; idx++){
        if(cb(elements[idx])){
            return elements[idx];
        }
    }
    return undefined;
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
}
module.exports=find;