function reduce(elements, cb, startingValue) {
    if (startingValue==undefined){
        startingValue=elements[0];
        for(let idx=1; idx<elements.length; idx++){
            startingValue=cb(startingValue, elements[idx], idx, elements);
        }
        return startingValue;
    }
    else{
        for(let idx=0; idx<elements.length; idx++){
            startingValue=cb(startingValue, elements[idx], idx, elements);
        }
        return startingValue;
    }
}
module.exports=reduce;