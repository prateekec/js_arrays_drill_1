function map(elements, cb) {
    let new_arr=[];
    for(let idx=0; idx<elements.length; idx++){
        new_arr.push(cb(elements[idx],idx,elements));
    }
    return new_arr;
}
module.exports=map;